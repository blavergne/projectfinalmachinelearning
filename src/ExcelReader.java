import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class ExcelReader {
	
	ExcelReader(){
	
		String fileName = "table.csv";
		File file = new File(fileName);
		
		try{
			Scanner inputStream = new Scanner(file);
			while(inputStream.hasNext()){
				String data = inputStream.next();
				String[] values = data.split(",");
				//values from index 0 - 7 store (Date,Open,High,Low,Close,Volume,Adj,Close)
				System.out.println(values[0]);
			}
			inputStream.close();
		} catch(FileNotFoundException e){
			e.printStackTrace();
		}
			
	}
	
}
