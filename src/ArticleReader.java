import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;


public class ArticleReader {

	private String data;
	
	ArticleReader(){
		
		String fileName = "article.txt";
		File file = new File(fileName);
		
		try{
			Scanner inputStream = new Scanner(file);
			while(inputStream.hasNext()){
				data += inputStream.next() + " ";
			}
			inputStream.close();
		} catch(FileNotFoundException e){
			e.printStackTrace();
		}
		
	}
	
	//get members
	public String getArticleString(){
		
		return data;
		
	}
	
}
