import java.util.*;

import edu.stanford.nlp.sentiment.SentimentCoreAnnotations;
import edu.stanford.nlp.dcoref.CorefChain;
import edu.stanford.nlp.dcoref.CorefCoreAnnotations.CorefChainAnnotation;
import edu.stanford.nlp.io.EncodingPrintWriter.out;
import edu.stanford.nlp.ling.CoreAnnotations.NamedEntityTagAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.PartOfSpeechAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TextAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.semgraph.SemanticGraph;
import edu.stanford.nlp.semgraph.SemanticGraphCoreAnnotations.CollapsedCCProcessedDependenciesAnnotation;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.TreeCoreAnnotations.TreeAnnotation;
import edu.stanford.nlp.util.CoreMap;

public class AZNFinText {

	public static void main(String args[]){
		
		Properties props = new Properties();
		ExcelReader er = new ExcelReader();
		ArticleReader ar = new ArticleReader();
		props.setProperty("annotators", "tokenize, ssplit, pos, lemma, ner, parse, sentiment, dcoref");
		StanfordCoreNLP pipeline = new StanfordCoreNLP(props);
		
		int sentimentScore = 0;
		
		String text = ar.getArticleString();
		
		Annotation document = new Annotation(text);

		pipeline.annotate(document);
		
		List<CoreMap> sentences = document.get(SentencesAnnotation.class);
		
		for(CoreMap sentence: sentences) {
		      // traversing the words in the current sentence
		      // a CoreLabel is a CoreMap with additional token-specific methods
		      for (CoreLabel token: sentence.get(TokensAnnotation.class)) {
		        // this is the text of the token
		        String word = token.get(TextAnnotation.class);
		        // this is the POS tag of the token
		        String pos = token.get(PartOfSpeechAnnotation.class);
		        // this is the NER label of the token
		        String ne = token.get(NamedEntityTagAnnotation.class);
		        String sent = token.get(SentimentCoreAnnotations.SentimentClass.class);
		        //System.out.println(word);
		        //System.out.println(pos);
		        //System.out.println(ne);
		        //System.out.println(sent);
		        //System.out.println();
		        
		        if(pos.equals("NNP") && ne.equals("ORGANIZATION")){
		        	System.out.println(word);
		        }
		        
		        switch(sent){
		        
		        	case "Positive": sentimentScore += 1; break;
		        	case "Very Positive": sentimentScore += 2; break;
		        	case "Negative": sentimentScore -= 1; break;
		        	case "Very Negative": sentimentScore -= 2; break;
		        	default: sentimentScore += 0;
		        	
		        }
		        
		      }
     
		      // this is the parse tree of the current sentence
		      Tree tree = sentence.get(TreeAnnotation.class);
		      
		      // this is the Stanford dependency graph of the current sentence
		      SemanticGraph dependencies = sentence.get(CollapsedCCProcessedDependenciesAnnotation.class);
		    }
		
			if(sentimentScore > -5 && sentimentScore < 5){
		 		System.out.println("Neutral Sentiment");
			}
			else if(sentimentScore < -5){
				System.out.println("Negative Sentiment");
			}
			else{
				System.out.println("Positive Sentiment");
			}
			
		    // This is the coreference link graph
		    // Each chain stores a set of mentions that link to each other,
		    // along with a method for getting the most representative mention
		    // Both sentence and token offsets start at 1!
		    Map<Integer, CorefChain> graph = document.get(CorefChainAnnotation.class);
		
		    
	}
	
}
